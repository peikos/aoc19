# AoC19

[![Hackage](https://img.shields.io/hackage/v/AoC19.svg)](https://hackage.haskell.org/package/AoC19)
[![MIT license](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)
[![Stackage Lts](http://stackage.org/package/AoC19/badge/lts)](http://stackage.org/lts/package/AoC19)
[![Stackage Nightly](http://stackage.org/package/AoC19/badge/nightly)](http://stackage.org/nightly/package/AoC19)

See README for more info